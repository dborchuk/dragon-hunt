define(() ->
  DIRECTION = {
    UP: 'UP'
    DOWN: 'DOWN'
    LEFT: 'LEFT'
    RIGHT: 'RIGHT'
  }
  RIVER_TYPE = {
    LEFT_TOP_AND_RIGHT_BOTTOM: 'LEFT_TOP_AND_RIGHT_BOTTOM'
    LEFT_BOTTOM_AND_RIGHT_TOP: 'LEFT_BOTTOM_AND_RIGHT_TOP'
    LEFT_RIGHT_AND_BOTTOM_TOP: 'LEFT_RIGHT_AND_BOTTOM_TOP'
  }
  map = null
  player = null
  dragon = null
  holes = null
  rivers = null
  config = null
  options = {}
  statistics = {
    win: 0
    loss: 0
  }
  visited = {}

  getRelativeCoords = (width, height, x, y, xOffset, yOffset) ->
    if x + xOffset > width - 1
      newX = x + xOffset - width
    else if x + xOffset < 0
      newX =  width + (x + xOffset)
    else
      newX = x + xOffset

    if y + yOffset > height - 1
      newY = y + yOffset - height
    else if y + yOffset < 0
      newY =  height + (y + yOffset)
    else
      newY = y + yOffset

    return {
      x: newX
      y: newY
    }

  generateArea = (map, object, steps, prop) ->
    newCoords = getNextCoords(map, object, DIRECTION.UP)
    map[newCoords.x][newCoords.y][prop] = true
    if steps > 1
      generateArea(map, newCoords, steps - 1, prop)

    newCoords = getNextCoords(map, object, DIRECTION.RIGHT)
    map[newCoords.x][newCoords.y][prop] = true
    if steps > 1
      generateArea(map, newCoords, steps - 1, prop)

    newCoords = getNextCoords(map, object, DIRECTION.DOWN)
    map[newCoords.x][newCoords.y][prop] = true
    if steps > 1
      generateArea(map, newCoords, steps - 1, prop)

    newCoords = getNextCoords(map, object, DIRECTION.LEFT)
    map[newCoords.x][newCoords.y][prop] = true
    if steps > 1
      generateArea(map, newCoords, steps - 1, prop)

  getNextCoords = (map, coords, direction, stepCb) ->
    width = map.length
    height = map[0].length
    result = null

    switch direction
      when DIRECTION.UP
        result = getRelativeCoords(width, height, coords.x, coords.y, 0, -1)
        tile = map[result.x][result.y]
        if tile.river
          switch tile.river.type
            when RIVER_TYPE.LEFT_TOP_AND_RIGHT_BOTTOM
              result = getNextCoords(map, result, DIRECTION.RIGHT, stepCb)
            when RIVER_TYPE.LEFT_BOTTOM_AND_RIGHT_TOP
              result = getNextCoords(map, result, DIRECTION.LEFT, stepCb)
            when RIVER_TYPE.LEFT_RIGHT_AND_BOTTOM_TOP
              result = getNextCoords(map, result, DIRECTION.UP, stepCb)
      when DIRECTION.RIGHT
        result = getRelativeCoords(width, height, coords.x, coords.y, 1, 0)
        tile = map[result.x][result.y]
        if tile.river
          switch tile.river.type
            when RIVER_TYPE.LEFT_TOP_AND_RIGHT_BOTTOM
              result = getNextCoords(map, result, DIRECTION.UP, stepCb)
            when RIVER_TYPE.LEFT_BOTTOM_AND_RIGHT_TOP
              result = getNextCoords(map, result, DIRECTION.DOWN, stepCb)
            when RIVER_TYPE.LEFT_RIGHT_AND_BOTTOM_TOP
              result = getNextCoords(map, result, DIRECTION.RIGHT, stepCb)
      when DIRECTION.DOWN
        result = getRelativeCoords(width, height, coords.x, coords.y, 0, 1)
        tile = map[result.x][result.y]
        if tile.river
          switch tile.river.type
            when RIVER_TYPE.LEFT_TOP_AND_RIGHT_BOTTOM
              result = getNextCoords(map, result, DIRECTION.LEFT, stepCb)
            when RIVER_TYPE.LEFT_BOTTOM_AND_RIGHT_TOP
              result = getNextCoords(map, result, DIRECTION.RIGHT, stepCb)
            when RIVER_TYPE.LEFT_RIGHT_AND_BOTTOM_TOP
              result = getNextCoords(map, result, DIRECTION.DOWN, stepCb)
      when DIRECTION.LEFT
        result = getRelativeCoords(width, height, coords.x, coords.y, -1, 0)
        tile = map[result.x][result.y]
        if tile.river
          switch tile.river.type
            when RIVER_TYPE.LEFT_TOP_AND_RIGHT_BOTTOM
              result = getNextCoords(map, result, DIRECTION.DOWN, stepCb)
            when RIVER_TYPE.LEFT_BOTTOM_AND_RIGHT_TOP
              result = getNextCoords(map, result, DIRECTION.UP, stepCb)
            when RIVER_TYPE.LEFT_RIGHT_AND_BOTTOM_TOP
              result = getNextCoords(map, result, DIRECTION.LEFT, stepCb)

    if (typeof stepCb) == 'function'
      stepCb(coords)

    return result

  generateHoles = (map, holesNumber) ->
    width = map.length
    height = map[0].length
    success = false

    if !holes
      holes = []
      i = 0
      while i < holesNumber
        while !success
          hole = {
            x: _.random(0, width - 1)
            y: _.random(0, height - 1)
          }
          if !map[hole.x][hole.y].occupied
            success = true
            holes.push(hole)
        i++
        success = false

    i = 0
    while i < holes.length
      hole = holes[i]
      map[hole.x][hole.y].hole = hole
      map[hole.x][hole.y].grass = true
      map[hole.x][hole.y].occupied = true
      generateArea(map, hole, 1, 'grass')
      i++

  generateDragon = (width, height, map, stepsToDragon) ->
    if !dragon
      dragon = {
        x: _.random(0, width - 1)
        y: _.random(0, height - 1)
      }
    map[dragon.x][dragon.y].dragon = dragon
    map[dragon.x][dragon.y].occupied = true
    generateArea(map, dragon, stepsToDragon, 'badSoil')

  generatePlayer = (map) ->
    width = map.length
    height = map[0].length
    success = false

    if !player
      while !success
        player = {
          x: _.random(0, width - 1)
          y: _.random(0, height - 1)
        }
        if !map[player.x][player.y].occupied
          success = true

    map[player.x][player.y].player = player
    map[player.x][player.y].occupied = true
    map[player.x][player.y].visited = true
    visited[player.x + ',' + player.y] = true

  generateMap = (width, height, holes, riverNumbers) ->
    stepsToDragon = 2
    map = []

    i = 0
    while i < width
      map.push []
      j = 0
      while j < height
        map[i].push {
          x: i
          y: j
          visited: visited[i + ',' + j]
        }
        j++
      i++

    generateRiver(map, riverNumbers)
    generateDragon(width, height, map, stepsToDragon)
    generateHoles(map, holes)
    generatePlayer(map)

    map

  generateRiver = (map, riverNumbers) ->
    width = map.length
    height = map[0].length
    success = false

    if !rivers
      rivers = []
      i = 0
      while i < riverNumbers
        while !success
          river = {
            x: _.random(0, width - 1)
            y: _.random(0, height - 1)
          }
          if !map[river.x][river.y].occupied
            success = true
            river.type = createRiverType()
            rivers.push(river)
        i++
        success = false

    i = 0
    while i < rivers.length
      river = rivers[i]
      map[river.x][river.y].river = river
      map[river.x][river.y].occupied = true
      i++

  createRiverType = () ->
    switch _.random(0, 2)
      when 0
        result = RIVER_TYPE.LEFT_TOP_AND_RIGHT_BOTTOM
      when 1
        result = RIVER_TYPE.LEFT_BOTTOM_AND_RIGHT_TOP
      when 2
        result = RIVER_TYPE.LEFT_RIGHT_AND_BOTTOM_TOP

    return result

  initGame = (config) ->
    config = config or {}
    options.width = config.width or 10
    options.height = config.height or 10
    options.holesNumber = if config.holes? then config.holes else 1
    options.riverNumbers = if config.rivers? then config.rivers else 1

    player = null
    dragon = null
    holes = null
    rivers = null
    visited = {}

    return

  movePlayer = (direction) ->
    width = map.length
    height = map[0].length
    stepCb = (coords) ->
      visited[coords.x + ',' + coords.y] = true

    newCoords = getNextCoords(map, player, direction, stepCb)

    if !checkLoss(map, newCoords)
      player.x = newCoords.x
      player.y = newCoords.y
      visited[newCoords.x + ',' + newCoords.y] = true
    else
      loss()
      initGame(options)

  checkLoss = (map, newCoords) ->
    return map[newCoords.x][newCoords.y].dragon or map[newCoords.x][newCoords.y].hole

  getMap = () ->
    map = generateMap(options.width, options.height, options.holesNumber, options.riverNumbers)

  checkWin = (map, aimCoords) ->
    return map[aimCoords.x][aimCoords.y].dragon

  fire = (x, y) ->
    if checkWin(map, {x: x, y: y})
      win()
      return true
    else
      loss()
      return false

  win = () ->
    statistics.win++
    showWinMsg()

  loss = () ->
    statistics.loss++
    showLossMsg()

  showWinMsg = () ->
    console.clear()
    log '+----------+'
    log '| You win! |'
    log '+----------+'
    showStatistic statistics

  showLossMsg = () ->
    console.clear()
    log '+-----------+'
    log '| You lost! |'
    log '+-----------+'
    showStatistic statistics

  showStatistic = (statistics) ->
    log 'Statistics:'
    log '    win:  ' + statistics.win
    log '    loss: ' + statistics.loss

  {
    init: initGame
    getMap: getMap
    move: movePlayer
    fire: fire
    DIRECTION: DIRECTION
  }
)