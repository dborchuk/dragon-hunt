window.log = (msg) ->
  console.log msg


gragonHunter = angular.module('gragonHunter', [])

# Game service
injectArray = ['$q']
gameProvider = ($q) ->
  GameCache = null
  options = {
    holes: 1
    width: 4
    height: 7
    rivers: 3
  }

  @init = () ->
    $q(
      (resolve, reject) ->
        require(
          ['scripts/game'],
          (Game) ->
            GameCache = Game
            Game.init(options)
            resolve()
        )
    )

  @fire = (x, y) ->
    GameCache.fire(x, y)

  @getMap = () ->
    if GameCache
      GameCache.getMap()

  @moveUp = () ->
    if GameCache
      GameCache.move(GameCache.DIRECTION.UP)

  @moveRight = () ->
    if GameCache
      GameCache.move(GameCache.DIRECTION.RIGHT)

  @moveDown = () ->
    if GameCache
      GameCache.move(GameCache.DIRECTION.DOWN)

  @moveLeft = () ->
    if GameCache
      GameCache.move(GameCache.DIRECTION.LEFT)

  return
gameProvider.$inject = injectArray
gragonHunter.service('gameProvider', gameProvider)

# Main ctrl
injectArray = ['gameProvider']
mainCtrl = (gameProvider) ->
  vm = this

  vm.keyUpHandler = (event) ->
    switch event.keyCode
      # Up
      when 38
        gameProvider.moveUp()
      # Down
      when 40
        gameProvider.moveDown()
      # Left
      when 37
        gameProvider.moveLeft()
      # Right
      when 39
        gameProvider.moveRight()

    vm.map = gameProvider.getMap()

  gameProvider.init().then () ->
    vm.map = gameProvider.getMap()

  vm.clickHandler = (cell) ->
    gameProvider.fire(cell.x, cell.y)
    gameProvider.init().then () ->
      vm.map = gameProvider.getMap()

  return
mainCtrl.$inject = injectArray
gragonHunter.controller('mainCtrl', mainCtrl)